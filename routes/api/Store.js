const Store = require('../../models/Store');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD INSERT
router.post('/', (req, res, next) => {
  Store.InsertStore(req.body).then((store) => {
    res.json(store);
  }).catch((err) => {
    next(err);
  })
});

// CRUD DELETE
router.delete('/:IdStore', (req, res, next) => {
  Store.DeleteStore(req.params.IdStore).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});

router.get('/:IdUser', (req, res, next) => {
  Store.GetAllStoresByUser(req.params.IdUser).then((stores) => {
    res.json(stores);
  }).catch((err)=>{
      next(err);
  })
});

// Retrieve information from t_Store
router.get('/', (req, res, next) => {
  Store.GetAllStores().then((stores) => {
    res.json(stores);
  }).catch((err)=>{
      next(err);
  })
});

router.get('/:idUser', (req, res, next) => {
  Store.getCurrentStore(req.params.idUser).then((stores) => {
    res.json(stores);
  }).catch((err)=>{
      next(err);
  })
});

router.get('/:usrr', (req, res, next) => {
  Store.getNotifyStore(req.params.usrr).then((stores) => {
    res.json(stores);
  }).catch((err)=>{
      next(err);
  })
});
  
  
  module.exports = router;