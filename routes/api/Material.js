const Material = require('../../models/Material');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD INSERT
router.post('/', (req, res, next) => {
  Material.InsertMaterial(req.body).then((material) => {
    res.json(material);
  }).catch((err) => {
    next(err);
  })
});

//Retrieve information from t_materials
router.get('/', cors(), (req, res, next) => {
  Material.GetAll().then((materials) => {
    res.json(materials);
  }).catch((err)=>{
      next(err);
  })
});

// CRUD DELETE
router.delete('/:IdMaterial', (req, res, next) => {
  Material.DeleteMaterial(req.params.IdMaterial).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});

//by IdMaterial
router.get('/:Id', (req, res, next) => {
Material.GetMaterial(req.params.Id).then((materials) => {
  res.json(materials);
}).catch((err)=>{
    next(err);
  })
});

module.exports = router;
