const Roles = require('../../models/Role');

const express = require('express');
const router = express.Router();
const cors = require('cors');

//Retrieve information from t_Role
router.get('/', cors(), (req, res, next) => {
    Roles.GetAllRoles().then((roles) => {
      res.json(roles);
    }).catch((err)=>{
        next(err);
    })
  });

  module.exports = router;