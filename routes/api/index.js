var express = require('express');
var router = express.Router();

var warehousehistory = require('./WareHouseHistory');
var materials =  require('./Material');
var school =  require('./School');
var zone = require('./Zone');
var user = require('./User');
var role = require('./Role');
var store = require('./Store');
var warehouse = require('./Warehouse');
var config = require('./Config');
var order = require('./Order');
var report = require('./Report');
var notification = require('./Notification');
var auth = require('./Auth');
var authMiddleware = require('../../middleware/auth');

/* GET home page. */
router.use('/auth', auth);
router.use(authMiddleware)
router.use('/materials', materials);
router.use('/schools', school);
router.use('/zones', zone);
router.use('/users', user);

router.use('/roles', role);
router.use('/stores', store);
router.use('/warehouse', warehouse);
router.use('/config', config);
router.use('/orders', order);
router.use('/reports', report);
router.use('/notifications', notification);
router.use('/warehousehistory', warehousehistory);

module.exports = router;