const Zones = require('../../models/Zone');

const express = require('express');
const router = express.Router();
const cors = require('cors');

//Retrieve information from t_Zone
router.get('/', cors(), (req, res, next) => {
    Zones.GetAll().then((zones) => {
      res.json(zones);
    }).catch((err)=>{
        next(err);
    })
  });

  module.exports = router;