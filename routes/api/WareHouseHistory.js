const Warehousehistory = require('../../models/WarehouseHistory');

const express = require('express');
const router = express.Router();
const cors = require('cors');

//Get rows to dialog
router.get('/:type', cors(), (req, res, next) => {
  Warehousehistory.getRows(req.query, req.params.type, (error, warehousehistory) => {
    if (error) {
      return next(error);
    }
    res.json(warehousehistory);
  })
});
  
module.exports = router;