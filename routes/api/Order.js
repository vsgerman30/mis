const Orders = require('../../models/Order');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// Retrieve
router.get('/', cors(), (req, res, next) => {
    Orders.GetOrders().then((orders) => {
        res.json(orders);
    }).catch((err) => {
        next(err);
    })
})

// CRUD INSERT
router.post('/', (req, res, next) => {
    Orders.InsertOrder(req.body, (error, odr) => {
      if (error) {
        return next(error);
      }
        res.json(odr);
    })
  });

module.exports = router;