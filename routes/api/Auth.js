const User = require('../../models/User');

const auth = require('../../middleware/auth');
const express = require('express');
const router = express.Router();
const cors = require('cors');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

//Retrieve information from t_Zone
router.post('/login', cors(), (req, res, next) => {
    User.FindUserByEmail(req.body.Email).then((user) => {
      if (!user) {
        return next(new Error('Usuario no encontrado'));
      }

      bcrypt.compare(req.body.Pwd, user.Pwd)
          .then(function(isMatch) {
            if (isMatch) {
              // console.log('  user', JSON.stringify(user), process.env.JWT_SECRET, ' JWT');
              jwt.sign(JSON.stringify(user), process.env.JWT_SECRET, function (error, token) {
                if(error) {
                  console.log(error, 'error sign');
                  return next(error);
                }

                res.json({token, user});
              });
            } else {
              return next(new Error('Contrasena invalida'));
            }
          });
    }).catch((err)=>{
        console.log('ERROR: ', err);
        next(err);
    })
  });

  module.exports = router;