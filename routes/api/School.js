const School = require('../../models/School');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD INSERT
router.post('/', (req, res, next) => {
  School.InsertSchool(req.body).then((school) => {
    res.json(school);
  }).catch((err) => {
    next(err);
  })
});

router.post('/patch', (req, res, next) => {
  School.updateWSchoolNot(req.body, (error, sh) => {
    if (error) {
      return next(error);
    }
    res.json(sh);
  })
});

// CRUD DELETE
router.delete('/:IdSchool', (req, res, next) => {
  School.DeleteSchool(req.params.IdSchool).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});

// Retrieve information from t_Schools
router.get('/:IdUser', (req, res, next) => {
    School.GetAllMByUser(req.params.IdUser).then((schools) => {
      res.json(schools);
    }).catch((err)=> {
      console.log(err);
        next(err);
    })
  });

  // Retrieve information from t_Schools
router.get('/', (req, res, next) => {
  School.GetAllSchools().then((schools) => {
    res.json(schools);
  }).catch((err)=> {
    console.log(err);
      next(err);
  })
});

  module.exports = router;
