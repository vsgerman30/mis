const Reports = require('../../models/Report');

const express = require('express');
const router = express.Router();
const cors = require('cors');

//Retrieve information from t_Role
router.get('/', cors(), (req, res, next) => {
  Reports.getReports(req.query, (error, reports) => {
    if (error) {
      return next(error);
    }
    res.json(reports);
  })
});

//Get rows to dialog
router.get('/:type', cors(), (req, res, next) => {
  Reports.getRows(req.query, (error, reports) => {
    if (error) {
      return next(error);
    }
    res.json(reports);
  })
});

module.exports = router;