const Warehouse = require('../../models/Warehouse');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD Create
router.post('/', (req, res, next) => {
  Warehouse.InsertWarehouse(req.body, (error, Warehouse) => {
    if (error) {
      return next(error);
    }
    res.json(Warehouse);
  })
});

router.post('/patch', (req, res, next) => {
  Warehouse.updateWarehouse(req.body, (error, wh) => {
    if (error) {
      return next(error);
    }
    res.json(wh);
  })
});

router.post('/bringback', (req, res, next) => {
  Warehouse.bringBack(req.body, (error2, wh2) => {
    if (error2) {
      return next(error2);
    }
    res.json(wh2);
  })
});

//CRUD Read
router.get('/:IdStore/:IdUser', cors(), (req, res, next) => {
  Warehouse.GetAllWareHouse(req.params.IdStore, req.params.IdUser).then((Warehouse) => {
    res.json(Warehouse);
  }).catch((err)=>{
      console.log(err);
      next(err);
  })
});

// CRUD Update
// Dont neecesary, validate Insert
router.put('/', cors(), (req, res, next) => {
  Warehouse.UpdateWarehouse(req.body).then((Warehouse) => {
    res.json(Warehouse);
  }).catch((err) => {
    next(err);
  });
});

// CRUD Delete
router.delete('/:IdWarehouse', (req, res, next) => {
  Warehouse.DeleteWarehouse(req.params.IdWarehouse).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});
  
module.exports = router;