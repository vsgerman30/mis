const Config = require('../../models/Config');

const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD INSERT
router.post('/', (req, res, next) => {
  Config.InsertConfig(req.body).then((config) => {
    res.json(config);
  }).catch((err) => {
    next(err);
  })
});

// CRUD DELETE
router.delete('/:IdConfig', (req, res, next) => {
    Config.DeleteConfig(req.params.IdConfig).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});

// Retrieve information from t_Users
router.get('/', (req, res, next) => {
    Config.GetAllConfig().then((config) => {
      res.json(config);
    }).catch((err)=>{
        next(err);
    })
  });

  module.exports = router;
