const Notification = require('../../models/Notification');

const express = require('express');
const router = express.Router();
const cors = require('cors');

//Retrieve information from t_Role
router.get('/:IdStore/:Type', cors(), (req, res, next) => {
  Notification.getNotification(req.params.IdStore, req.params.Type).then((notification) => {
    res.json(notification);
  }).catch((err)=>{
      next(err);
  })
});

router.get('/:IdNotification', cors(), (req, res, next) => {
  Notification.getNotById(req.params.IdNotification).then((Notification) => {
    res.json(Notification);
  }).catch((err)=>{
      next(err);
  })
});

router.get('/:IdUser/:IdStore/:type', cors(), (req, res, next) => {
  Notification.getNotify(req.params.IdUser).then((Notification) => {
    res.json(Notification);
  }).catch((err)=>{
      next(err);
  })
});

module.exports = router;