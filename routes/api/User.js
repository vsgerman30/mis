const User = require('../../models/User');

const auth = require('../../middleware/auth');
const express = require('express');
const router = express.Router();
const cors = require('cors');

// CRUD INSERT
router.post('/', (req, res, next) => {
  User.InsertUser(req.body).then((user) => {
    res.json(user);
  }).catch((err) => {
    console.log(err);
    next(err);
  })
});

// CRUD DELETE
router.delete('/:IdUser', (req, res, next) => {
  User.DeleteUser(req.params.IdUser).then(() => {
    res.json({success: true})
  }).catch((err) => {
    return next(err);
  })
});

// Retrieve information from t_Users
router.get('/', (req, res, next) => {
    User.GetAllUsers().then((users) => {
      res.json(users);
    }).catch((err)=>{
        next(err);
    })
  });

// Retrieve information from t_Users
router.get('/:IdRole', (req, res, next) => {
  User.GetUsersAdmin(req.params.IdRole).then((users) => {
    res.json(users);
  }).catch((err)=>{
      next(err);
  })
});

  module.exports = router;
