const jwt = require('jsonwebtoken')
const User = require('../models/User')

const auth = async(req, res, next) => {

    
    try {
        console.log('shingamos pare');
        const header = req.header('Authorization') || ' ';
        const token = header.replace('Bearer ', '')
        const data = jwt.verify(token, process.env.JWT_SECRET)

        const user = await User.FindUserByEmail(data.Email)
        if (!user) {
            throw new Error()
        }
        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).json({ error: 'Not authorized to access this resource' })
    }

}
module.exports = auth