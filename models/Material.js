const connection = require('./Connection');

// CRUD INSERT
function saveMaterial(description) {

    return connection('Material')
        .insert({
            Description : description.Description,
            Price : description.Price,
            Date : new Date(),
            Type : description.Type
        });
}

function updateMaterial(description, IdM) {
    return connection('Material')
        .update('Price', description.Price)
        .where('IdMaterial', IdM);
}

function InsertMaterial(description){

    return connection('Material').select(['IdMaterial'])
        .where({
            IdMaterial: description.IdMaterial || 0
        })
        .first()
        .then((result) => {
            
            if (result) {
                return updateMaterial(description, result.IdMaterial);
            }
            else {
                return saveMaterial(description);
            }
        })
        .then(result => {
            console.log('SUCCESS: ', result);
        });
    };

// CRUD DELETE
function DeleteMaterial(IdMaterial){
    // Remove the warehouse belongs to user
    return connection('Material')
            .where('IdMaterial', IdMaterial)
            .del();
}

// Functions to retrieve information    
function GetAll(){
    return connection.select('IdMaterial', 'Description', 'Price', 'Type')
                        .from('Material')
                        .orderBy('Description');
};


module.exports = {  
                    GetAll                   
                    ,InsertMaterial
                    ,DeleteMaterial
                };