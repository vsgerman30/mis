const connection = require('./Connection');

// CRUD Read    
function getNotification(IdStore, Type) {
    return connection.select('Notification.IdNotification',
                                'Notification.Date',
                                'Notification.type',
                                'Notification.Msg',
                                'Users.Name AS Owner',
                                'Notification.Active')
                        .from('Notification')
                        .leftJoin('Users', 'Notification.Owner', 'Users.IdUser')
                        .leftJoin('Stores', 'Notification.IdStore', 'Stores.IdStore')
                        .where('Stores.IdStore', IdStore)
                        .andWhere('Notification.Active', Type);
};

function getNotById(IdNotification) {

    return connection.select('Schools.Description AS desSchool', 'Notification.IdSchool', 'Zone.Description AS desZone', 'Notification.IdZona')
                        .from('Notification')
                        .leftJoin('Schools', 'Notification.IdSchool', 'Schools.IdSchool')
                        .leftJoin('Zone', 'Notification.IdZona', 'Zone.IdZona')
                        .where('Notification.IdNotification', IdNotification);
};

function getNotify(IdUser) {

    return connection.count({ a: 'active' })
                        .from('Notification')
                        .leftJoin('Config', 'Notification.IdStore', 'Config.IdStore')
                        .where('Notification.Active', 1)
                        .andWhere('Config.IdUser', IdUser);
};

module.exports = {  
    getNotification
    , getNotById
    , getNotify
};