const connection = require('./Connection');
const async = require('async');

// CRUD CREATE
function InsertWarehouse(description, next) {

    connection('WareHouseHistory').select('IdControl')
        .first()
        .orderBy('IdControl', 'desc')
        .then(function (IdControl) {

            IdControl = IdControl.IdControl || 0;
            IdControl = IdControl + 1;

            async.eachOf(description.Materials, (material, key, callback) => {
  
                material.IdMaterial = material.IdMaterial || 0;

                connection('WareHouse').select(['IdWarehouse', 'Qty'])
                    .where({
                        IdMaterial: material.IdMaterial
                    })
                    .andWhere({
                        IdStore: description.IdStore
                    })
                    .andWhere({
                        IdUser: description.IdUser
                    })
                    .first()
                    .then(function (result) {

                        result = result || 0;

                        if (result >= 0) {
                            console.log('Not exist, like a chubby girl in slide')
                            insertMaterial(material, description.IdUser, description.IdStore, IdControl);

                        }
                        else {
                            const newQty = Number(result.Qty) + Number(material.Qty);
                            updateMaterial(newQty, result.IdWarehouse, description.IdStore, IdControl, description.IdUser, material);
                        }

                        callback();
                    })
                    .catch(err =>  {
                        console.log(err, '   rrrrrrrrrrrrrrrrrrrrrr');
                        callback(err);
                    });
            }, (error) => {
                if(error) {
                    return next(error);
                }
                next(null, description.Materials);
            });
        });
};

function updateWarehouse(description, next) {

    var idNotification = description.IdWarehouse;
    console.log(description, ' dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd');
    async.waterfall([
        function getDetailsTask1(callback) {

            connection('Notification')
                .select(['IdNotification', 'Qty', 'IdUser', 'IdMaterial', 'IdStore'])
                .where('IdNotification', description.IdWarehouse)
                .first()
                .then(function (details) {

                    callback(null, details);
                });
        },
        function getWarehouseTask2(detailsNot, callback) {

            connection('WareHouse')
                .select(['Qty'])
                .where('IdUser', detailsNot.IdUser)
                .andWhere('IdMaterial', detailsNot.IdMaterial)
                .andWhere('IdStore', detailsNot.IdStore)
                .first()
                .then(function (qtyWH) {
                    console.log(qtyWH, 'qtyResult');
                    detailsNot.Qty = detailsNot.Qty + Number(qtyWH.Qty);

                    callback(null, detailsNot);
                });
        },
        function updateWareHouseTask3(detailsWah, callback) {

            connection('WareHouse')
                .update('Qty', detailsWah.Qty)
                .where('IdMaterial', detailsWah.IdMaterial)
                .andWhere('IdUser', detailsWah.IdUser)
                .then((response) => {

                    callback(null, response);
                })
        },
        function endNotificationTask4(res, callback) {
            
            connection('Notification')
                .update('Active', false)
                .where('IdNotification', description.IdWarehouse)
                .then(function (res) {
                    callback();
                })
        }
    ],
    function (err) {
            next(err, idNotification);
    });
};

function bringBack(description, next) {

    console.log(description, ' ddddddddddddescriptionBringBack');
    connection('WareHouseHistory').select('IdControl')
        .first()
        .orderBy('IdControl', 'desc')
        .then(function (IdControl) {

            IdControl = IdControl.IdControl || 0;
            IdControl = IdControl + 1;

            async.eachOf(description.Materials, (material, key, callback) => {

                material.IdMaterial = material.IdMaterial || 0;
                var keeper = material.Qty;

                async.waterfall([
                    function getWarehouseTask2(callback) {
                        connection('WareHouse')
                            .select(['Qty'])
                            .where('IdUser', description.IdUser)
                            .andWhere('IdMaterial', material.IdMaterial)
                            .andWhere('IdStore', description.IdStore)
                            .first()
                            .then(function (qtyWH) {
                                console.log(qtyWH, 'qtyresultfrom warehouse')
                                material.Qty = Number(qtyWH.Qty) - material.Qty;
                                callback(null, material);
                            });
                    },
                    function updateWareHouseTask3(detailsWah, callback) {
                        console.log(detailsWah, ' obj to update');
                        connection('WareHouse')
                            .update('Qty', detailsWah.Qty)
                            .where('IdMaterial', detailsWah.IdMaterial)
                            .andWhere('IdUser', description.IdUser)
                            .andWhere('IdStore', description.IdStore)
                            .then((response) => {
                                callback(null, material);
                            })
                    },
                    function updateWareHouseTask3(detailsWah, callback) {
                        connection('WareHouseHistory')
                            .insert({
                                IdStore : description.IdStore,
                                IdControl: IdControl,
                                IdUser : description.IdUser,
                                IdMaterial : material.IdMaterial,
                                Qty: keeper,
                                Date : new Date(),
                                type: 'R'
                    })
                    .then(result => {
                        callback();
                    });
                    }
                ],
                function (err) {
                        callback(err, null);
                });
        }, (error) => {
            if(error) {
                return next(error);
            }
            next(null, description.Materials);
        });
    });
};

function updateMaterial(Qty, IdWarehouse, IdStore, IdControl, IdUser, description) {

    connection('WareHouse')
        .update('Qty', Qty)
        .where('IdWarehouse', IdWarehouse)
        .then(result => {
            
            connection('WareHouseHistory')
                .insert({
                    IdStore : IdStore,
                    IdControl: IdControl,
                    IdUser : IdUser,
                    IdMaterial : description.IdMaterial,
                    Qty: description.Qty,
                    Date : new Date(),
                    type: 'I'
                })
                .then(result => {
                    console.log('SUCCESS: updating record in WareHouseHistory');
                });
        });
}

function insertMaterial(description, IdUser, IdStore, IdControl) {

    connection('WareHouse')
        .insert({
            IdUser : IdUser,
            IdMaterial : description.IdMaterial,
            Qty: description.Qty,
            IdStore : IdStore,
            date : new Date()
        })
        .then(result => {

            connection('WareHouseHistory')
                .insert({
                    IdStore : IdStore,
                    IdControl: IdControl,
                    IdUser : IdUser,
                    IdMaterial : description.IdMaterial,
                    Qty: description.Qty,
                    Date : new Date(),
                    type: 'I'
                })
                .then(result => {
                    console.log('SUCCESS: insert new record in WareHouseHistory');
                });
        });
}

// CRUD Read    
function GetAllWareHouse(IdStore, IdUser) {

    return connection.select('WareHouse.IdWarehouse', 'Material.Description AS IdMaterial', 'WareHouse.Qty', 'Users.Name', 'Stores.Description AS IdStore')
                        .from('WareHouse')
                        .leftJoin('Material', 'WareHouse.IdMaterial', 'Material.IdMaterial')
                        .leftJoin('Users', 'WareHouse.IdUser', 'Users.IdUser')
                        .leftJoin('Stores', 'WareHouse.IdStore', 'Stores.IdStore')
                        .where('Stores.IdStore', IdStore)
                        .andWhere('WareHouse.IdUser', IdUser)
                        .orderBy('Material.Description');
};

// CRUD DELETE
function DeleteWarehouse(IdWarehouse) {
    // remove the schools to belong that user
    // Remove the warehouse belongs to user
    return connection('WareHouse')
            .where('IdWarehouse', IdWarehouse)
            .del();
}

module.exports = {  
    GetAllWareHouse
    , InsertWarehouse
    , DeleteWarehouse
    , updateWarehouse
    , bringBack
};