const connection = require('./Connection');

// CRUD INSERT
function InsertStore(description){

    return connection('Zone').select(['IdZona'])
        .where('Description', description.IdZona)
        .first()
        .then(function (IdZona) {

            IdZona = IdZona || 0;

            if (IdZona === 0) {
                connection('Zone')
                    .insert({
                        Description: description.IdZona,
                        IdUser: 0
                    }).returning('IdZona')
                    .then((res) => {
                        const [IdZone] = res;
                        validateStore(description, IdZone);
                    });
            } else {
                validateStore(description, IdZona.IdZona);
            }
        });
}

function validateStore(description, IdZone) {

    return connection('Stores').select(['IdStore'])
        .where('Description', description.Description)
        .first()
        .then(function (IdStore) {

            IdStore = IdStore || 0;

            if (IdStore >= 1) {
                console.log('This store already exists!');
            } else {
                connection('Stores')
                    .insert({
                        Description: description.Description,
                        Date: new Date(),
                        IdZone: IdZone,
                        remarks: description.Remarks,
                        isPrivate: description.isPrivate,
                        owner: description.owner
                    })
                    .returning('IdStore')
                    .then((idStore) => {
                        const [IdStore] = idStore;
                        if (description.owner > 0) {
                            connection('Config')
                                .insert({
                                    IdUser: description.owner,
                                    IdStore: IdStore,
                                    Date: new Date()
                                })
                                .then((res) => {
                                    console.log('SUCCESS: insert ', res);
                                })
                        }
                        console.log('SUCCESS: insert ', result);
                    });
            }
        });
}

// CRUD DELETE
function DeleteStore(IdStore){
    return connection('Stores')
            .where('IdStore', IdStore)
            .del();
}

// Functions to retrieve information    
function GetAllStoresByUser(IdUser){
    return connection.select('Stores.IdStore' ,'Stores.Description AS Description', 'Zone.Description AS IdZone', 'Stores.remarks')
                        .from('Stores')
                        .leftJoin('Config', 'Stores.IdStore', 'Config.IdStore')
                        .leftJoin('Zone', 'Stores.IdZone', 'Zone.IdZona')
                        .leftJoin('Users', 'Config.IdUser', 'Users.IdUser')
                        .where('Users.IdUser', IdUser)
                        .andWhere('Stores.isPrivate', 0)
                        .orWhere('Stores.owner', IdUser)
                        .orWhere('Config.IdUser', IdUser);
}

function GetAllStores(){
    return connection.select('Stores.IdStore' ,'Stores.Description AS Description', 'Zone.Description AS IdZone', 'Stores.remarks')
                        .from('Stores')
                        .leftJoin('Zone', 'Stores.IdZone', 'Zone.IdZona')
                        .where('isPrivate', 0);
}

// Functions to retrieve information    
function getCurrentStore(IdUser){
    return connection.select('Config.IdStore', 'Stores.Description')
                        .from('Config')
                        .leftJoin('Stores', 'Stores.IdStore', 'Config.IdStore')
                        .where('IdUser', IdUser)
}

// Functions to retrieve information by Notify    
function getNotifyStore(IdUser){
    return connection.select('Config.IdStore', 'Stores.Description')
                        .from('Config')
                        .leftJoin('Stores', 'Stores.IdStore', 'Config.IdStore')
                        .where('Config.IdUser', IdUser)
}

module.exports = {  
    GetAllStores
    , InsertStore
    , DeleteStore
    , getCurrentStore
    , GetAllStoresByUser
    , getNotifyStore
};