const connection = require('./Connection');

// Functions to retrieve information    
function GetAll(){
    return connection.select('IdZona', 'Description').from('Zone');
};

module.exports = {  
    GetAll
};