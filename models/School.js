const connection = require('./Connection');
const async = require('async');


// CRUD INSERT
function InsertSchool(description){

    return connection('Zone').select(['IdZona'])
        .where({
            Description: description.IdZona
        })
        .first()
        .then((IdZona) => {

            IdZona = IdZona || 0;
            if (IdZona >= 0) {
                connection('Zone')
                    .insert({
                        IdUser: description.IdUser,
                        Description: description.IdZona
                    }).returning('IdZona')
                    .then((res) => {

                        const [IdZone] = res;
                        validateSchool(description, IdZone).then((r) => {
                            console.log('SUCCESS: ', r);
                        });
                    });
            }
            else {
                validateSchool(description, IdZona.IdZona).then((r) => {
                    console.log('SUCCESS: ', r);
                });
            }
        });
}

function updateWSchoolNot(description, next) {

    var idNotification = description.IdNotification;
    var idSchool;
    async.waterfall([
        function getDetailsTask1(callback) {
            connection('Notification')
                .select(['IdNotification', 'IdSchool', 'IdZona'])
                .where('IdNotification', description.IdNotification)
                .first()
                .then(function (details) {

                    callback(null, details);
                });
        },
        function updateSchoolTask3(detailsNot, callback) {

            idSchool = detailsNot.IdSchool;

            connection('Schools')
                .update('IdUser', description.IdUser)
                .where('IdSchool', detailsNot.IdSchool)
                .then((response) => {

                    callback(null, response);
                })
        },
        function updateOrdersTask4(rest, callback) {

            connection('Orders')
                .update('IdUser', description.IdUser)
                .where('IdSchool', idSchool)
                .then(function (res) {
                    callback(null, res);
                })
        },
        function findStoreTask5(result, callback) {

            connection('Orders')
                .select(['IdOrder', 'IdStore'])
                .where('IdSchool', idSchool)
                .first()
                .then(function (idStore) {
                    callback(null, idStore.IdOrder, idStore.IdStore);
                });
        },
        function updateWareHouseTask6(idorder, store, callback) {

            connection('OrderItems')
                .select(['IdMaterial', 'Qty'])
                .where('IdOrder', idorder)
                .then(function (materialsResult) {

                async.each(materialsResult, function(mat, callback) {
                    var idObj = mat.IdMaterial;
                    var qtyObj = mat.Qty;

                    connection('WareHouse')
                        .select(['Qty'])
                        .where('IdMaterial', idObj)
                        .andWhere('IdStore', store)
                        .andWhere('IdUser', description.IdUser)
                        .first()
                        .then(function (qtyResult) {
                            qtyResult = Number(qtyResult.Qty - qtyObj);

                            if (qtyResult >= 0) {
                                updateWareHouse(qtyResult, idObj, store, description.IdUser);
                            } else {
                                connection('WareHouse')
                                    .select(['WareHouse.Qty', 'WareHouse.IdUser', 'Material.Description'])
                                    .where('WareHouse.IdMaterial', idObj)
                                    .andWhere('WareHouse.IdStore', store)
                                    .andWhere('WareHouse.Qty', '>', '1')
                                    .leftJoin('Material', 'Material.IdMaterial', 'WareHouse.IdMaterial')
                                    .first()
                                    .then(function (helpResult) {
                                        var newQty = Number(helpResult.Qty - qtyObj);
                                        updateWareHouse(newQty, idObj, store, helpResult.IdUser);
                                        var Msg = 'La Notificacion de la order: ' + idorder + ' requirio tomar: ' + helpResult.Description + ' material(es) de ' + helpResult.Description + ' propiedad de otro administrador.';
                                        createNotification(idorder, idObj, null, null, qtyObj, helpResult.IdUser, store, 'Material', Msg, null);
                                    })
                             }
                            callback();
                        })
                        .catch(err =>  {
                            console.log(err, '   rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr');
                            callback(err);
                        });
                });
                callback(null, 0);
            }, function(err) {
                if( err ) {

                  console.log('A mat failed to process');
                } else {
                  console.log('All mats have been processed successfully');
                }
            });
        },
        function endNotificationTask4(r, callback) {
 
            connection('Notification')
                .update('Active', false)
                .where('IdNotification', idNotification)
                .then(function (r) {
                    callback();
                })
        }
    ],
    function (err) {
            next(err, idNotification);
    });
};

function updateWareHouse(Qty, IdMaterial, IdStore, IdUser) {

    return connection('WareHouse')
       .update('Qty', Qty)
       .where('IdMaterial', IdMaterial)
       .andWhere('IdUser', IdUser)
       .andWhere('IdStore', IdStore)
       .then(result => {
           console.log('SUCCESS: update ', result);
       });;
}

function createNotification(IdOrder, IdMaterial, IdSchool, IdZona, Qty, IdUser, IdStore, type, Msg, ownerSchool) {

    connection('Notification')
       .insert({
           IdOrder: IdOrder,
           IdMaterial: IdMaterial,
           IdSchool: IdSchool,
           IdZona: IdZona,
           Qty: Qty,
           IdUser: IdUser,
           IdStore: IdStore,
           Date: new Date(),
           type: type,
           Msg: Msg,
           Owner: ownerSchool,
           Active: true
       })
       .then(result => {
           console.log('SUCCESS: Notification', result);
       });;
}

function validateSchool(description, IdZone) {

    return connection('Schools').select(['IdSchool'])
        .where('Description', description.Description)
        .andWhere('IdZona', IdZone)
        .first()
        .then(function (IdSchool) {
            IdSchool = IdSchool || 0;
            if (IdSchool === 0){
                console.log(' Nada ceros')
            }
            else {
                IdSchool = IdSchool.IdSchool;
            }

            if(IdSchool >= 1) {
                console.log('This school already exists!');
            } else {
                connection('Schools')
                    .insert({
                        IdUser : description.IdUser,
                        IdZona : IdZone,
                        Description : description.Description,
                        Date : new Date()
                    })
                    .then(result => {
                        console.log('SUCCESS: insert ', result);
                    });
            }
        });
}

// CRUD DELETE
function DeleteSchool(IdSchool){
    return connection('Schools')
            .where('IdSchool', IdSchool)
            .del();
}

// Functions to retrieve information    
function GetAllMByUser(IdUser){ // Maybe this function will be removed.
    return connection.select('Schools.IdSchool', 'Schools.Description AS Description', 'Zone.Description AS ZDescription', 'Users.Name')
                        .from('Schools')
                        .leftJoin('Zone', 'Schools.IdZona', 'Zone.IdZona')
                        .leftJoin('Users', 'Schools.IdUser', 'Users.IdUser')
                        .where('Schools.IdUser', IdUser);
};

function GetAllSchools(){
    
    return connection.select('Schools.IdSchool', 'Schools.Description', 'Zone.Description AS ZDescription')
                        .from('Schools')
                        .leftJoin('Zone', 'Schools.IdZona', 'Zone.IdZona')
                        .orderBy('Schools.Description');
}

module.exports = {  
    InsertSchool
    , DeleteSchool
    , GetAllSchools
    , updateWSchoolNot
    , GetAllMByUser
};