const connection = require('./Connection');
const bcrypt = require('bcryptjs');

// CRUD INSERT
function InsertUser(description){
    var IdUser;
    // const crypt = new Promise(function(resolve, reject) {
    //     bcrypt.hash(description.Pwd, 10)
    // });
    return bcrypt.genSalt(10)
            .then((salt) => {
                return bcrypt.hash(description.Pwd, salt)   
            })
            .then(function(hash) {
                return connection('Users')
                    .insert({
                        Email: description.Email,
                        Name : description.Promotor,
                        Pwd : hash,
                        Date : new Date(),
                        IdRole : description.IdRole.IdRole
                    });
            })
            .then((result) => {
                if (description.Store) {
                    connection('Users').select(['IdUser'])
                        .where('Email', description.Email)
                        .andWhere('Name', description.Promotor)
                        .first()
                        .then((user) => {
                            return connection('Config')
                                .insert({
                                    IdUser: user.IdUser,
                                    IdStore: description.Store,
                                    Date: new Date()
                                });
                        });
                } else {
                    return 0;
                }
            });
}

// CRUD DELETE
function DeleteUser(IdUser){
    // remove the schools to belong that user
    // Remove the warehouse belongs to user
    return connection('Users')
            .where('IdUser', IdUser)
            .del();
}

// Functions to retrieve information    
function GetAllUsers(){
    return connection.select('Users.IdUser', 'Users.Name', 'Roles.Description', 'Users.Email')
                        .from('Users')
                        .leftJoin('Roles', 'Users.IdRole', 'Roles.IdRole')
                        .whereNot('Roles.IdRole', 3);
}

// Functions to retrieve information    
function FindUserByEmail(email){
    return connection.select('Users.IdUser', 'Users.Name', 'Users.Email', 'Users.IdRole', 'Users.Pwd')
                        .from('Users')
                        .first()
                        .where('Email', email);
}

// Functions to retrieve information    
function GetUsersAdmin(IdRole){

    return connection('Users').where({
        IdRole: 1
      }).orWhere({IdRole: 0})
      .select('*')
}

module.exports = {  
    GetAllUsers
    , InsertUser
    , DeleteUser
    , GetUsersAdmin
    , FindUserByEmail
};