const connection = require('./Connection');

// CRUD INSERT
function InsertConfig(description) {

    return connection('Config').select('IdConfig')
        .where({
            IdUser: description.IdUser.IdUser
        })
        .andWhere({
            IdStore: description.IdStore.IdStore
        }).then(function(result) {
            if (result.length === 0){
                const dateSave = new Date();
                    connection('Config')
                        .insert({
                            IdUser : description.IdUser.IdUser,
                            IdStore : description.IdStore.IdStore,
                            date : dateSave,
                    })
                    .then(result => {
                        console.log('SUCCESS: insert ', result);
                    });
            }
            else {
                console.log('This Config already exist')
            }
    });  
}


// Validate Insertr
function ValidateInsert(description) {
    const dateSave = new Date();
        return connection('Config')
                .insert({
                    IdUser : description.IdUser.IdUser,
                    IdStore : description.IdStore.IdStore,
                    date : dateSave,
                });
                    
}
    

// CRUD DELETE
function DeleteConfig(IdConfig){
    // remove the schools to belong that user
    // Remove the warehouse belongs to user
    return connection('Config')
            .where('IdConfig', IdConfig)
            .del();
}


// Functions to retrieve information    
function GetAllConfig(){

    return connection.select('Config.IdConfig', 'Users.Name AS IdUser', 'Stores.Description AS IdStore', 'Config.Date')
                        .from('Config')
                        .leftJoin('Users', 'Config.IdUser', 'Users.IdUser')
                        .leftJoin('Stores', 'Config.IdStore', 'Stores.IdStore')
                        .where('Stores.IsPrivate', 0);
};

module.exports = {  
    InsertConfig
    , DeleteConfig
    , GetAllConfig
};