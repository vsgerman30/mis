const connection = require('./Connection');
const async = require('async');

var matd = '';
var userd ='';

// Functions to retrieve information    
function GetOrders(){
    return connection.select('*')
                        .from('Orders');
};

// CRUD INSERT
function InsertOrder(description, next) {

    let OrderModel = 0;
    let ownerSchool = 0;
    async.waterfall([
        function getZoneTask1(callback) {

            connection('Zone').select(['IdZona'])
                .where('Description', description.IdZona)
                .first()
                .then(function (idZona) {

                    idZona = idZona || 0;
                    if (idZona !== 0) {
                        idZona = idZona.IdZona;
                    }

                    callback(null, idZona);
                })
                .catch(function (error) {
                    callback(error);
                });
        },
        function ZoneTask2(idZona, callback) {

            if (idZona >= 1) {
                callback(null, idZona);
            } else {
                connection('Zone')
                    .insert({
                        IdUser: null,
                        Description: description.IdZona,
                    })
                    .returning('IdZona')
                    .then((idZone) => {
                        const [IdZone] = idZone;
                        callback(null, IdZone);
                    })
                    .catch(function (error) {
                        callback(error);
                    });
            }
        },
        function getSchoolTask3(idZona, callback) {

            connection('Schools').select(['IdSchool'])
                .where('Description', description.IdSchool)
                .andWhere('IdZona', idZona)
                .first()
                .then(function (idSchool) {
                    idSchool = idSchool || 0;
                    if (idSchool !== 0) {
                        idSchool = idSchool.IdSchool;
                    }
                    callback(null, idSchool, idZona);
                })
                .catch(function (error) {
                    callback(error);
                });
        },
        function schoolExistsTask4(idSchool, idZona, callback) {

            if (idSchool >= 1) {
                description.IdSchool = idSchool;
                // Exists and get owner
                connection('Schools').select(['IdUser'])
                    .where('IdSchool', idSchool)
                    .first()
                    .then(function (idUser) {
                        ownerSchool = idUser || 0;
                        if (ownerSchool !== 0) {
                            ownerSchool = ownerSchool.IdUser;
                        }
                        callback(null, ownerSchool);
                    })
                    .catch(function (error) {
                        callback(error);
                    });
            } else {
                connection('Schools')
                    .insert({
                        IdUser: null,
                        IdZona: idZona,
                        Description: description.IdSchool,
                        Date: new Date()
                    })
                    .returning('IdSchool')
                    .then((IdSchools) => {
                        // call notification
                        const [IdSchool] = IdSchools;
                        var Msg = 'La escuela:  ' + description.IdSchool + ', de la zona: ' + description.IdZona + ' fue creada. Favor de asignar promotor';
                        createNotification(null, null, IdSchool, idZona, null, null, description.IdStore, 'Escuela', Msg, null);
                        description.IdSchool = IdSchool;
                        callback(null, 0);
                    })
                    .catch(function (error) {
                        callback(error);
                    });
            }
        },
        function insertOtrderTask6(ownerSchool, callback) {

            connection('Orders')
                .insert({
                    IdUser: ownerSchool,
                    IdSchool: description.IdSchool,
                    Student: description.Student,
                    Date: new Date(),
                    IdStore: description.IdStore 
                })
                .returning('IdOrder')
                .then((IdOrderOutput) => {
                    const [idOrder] = IdOrderOutput;
                    OrderModel = idOrder;
                    callback(null, idOrder);
                })
                .catch(function (error) {
                    callback(error);
                });
        },
        function getWarehouseTask7(idOrder, callback) {

            // Async each
            async.each(description.Details, function(mat, callback) {
                console.log(ownerSchool, '  super owwwwwwwwwwner')
                // if (ownerSchool > 0) {
                    connection('WareHouse').select(['Qty'] || [null])
                        .where('IdMaterial', mat.IdMaterial)
                        .andWhere('IdUser', ownerSchool)
                        .andWhere('IdStore', description.IdStore)
                        .first()
                        .then(function (dbQty) { 

                            var QtyWareHouse = dbQty || null;
                            if(QtyWareHouse !== null) {
                                QtyWareHouse = dbQty.Qty;
                            }

                            // create function to handle control material when a school doesnt have a owner *****
                            if((QtyWareHouse === null) || (QtyWareHouse === 0) || (mat.Qty > QtyWareHouse)) {
                                // means that user doesnt have materials
                                // and need take borrow
                                insertItems(mat.IdMaterial, idOrder, mat.Qty);
                                if (ownerSchool > 0) {
                                    if (QtyWareHouse >= 1) {
                                        updateWareHouse(mat.IdMaterial, 0, description.IdUser, description.IdStore);
                                    }
                                    let QtyBorrow = (mat.Qty - QtyWareHouse);
                                    getBorrowMaterials(description.IdStore, mat.IdMaterial, QtyBorrow, idOrder);
                                }
                            }
                            else {
                                // Enough materials qty and decrease own warehouse
                                insertItems(mat.IdMaterial, idOrder, mat.Qty);
                                if (ownerSchool > 0) {
                                    QtyWareHouse = Number(QtyWareHouse - mat.Qty);
                                    updateWareHouse(mat.IdMaterial, QtyWareHouse, ownerSchool, description.IdStore);
                                }
                            }
                        });
                //}

                callback();

            }, function(err) {
                if( err ) {

                  console.log('A mat failed to process');
                } else {
                  console.log('All mats have been processed successfully');
                }
            });

            callback();
        }
    ],
    function (err) {
        next(err, OrderModel);
    });
}

function getBorrowMaterials(IdStore, IdMaterial, QtyReq, idOrder) {
    var owwn = null;
    connection('WareHouse').select(['Qty', 'IdUser'])
        .where({
            IdStore: IdStore
        })
        .andWhere({
            IdMaterial: IdMaterial
        })
        .first()
        .then(function (result) {
            if(result.Qty >= QtyReq) {
                // Meas the qty can be supplied
                let Qty = result.Qty - QtyReq;
                updateWareHouse(IdMaterial, Qty, result.IdUser, IdStore);
                getDescriptionMaterials(IdMaterial).then(function (r) {
                    getNameUsers(result.IdUser);
                    var Msg = 'La venta: ' + idOrder + ' requirio tomar: ' + QtyReq + ' material(es) de ' + r.Description + ' propiedad de ' + userd + '.';
    
                    createNotification(idOrder, IdMaterial, null, null, QtyReq, result.IdUser, IdStore, 'Material', Msg, owwn);
                });

            }
        })
        .catch((error) => {
            console.log(error);
        });
}

function insertItems(IdMaterial, IdOrder, Qty) {
     connection('OrderItems')
        .insert({
            IdOrder: IdOrder,
            IdMaterial: IdMaterial,
            Qty: Qty,
            Date: new Date()
        })
        .then(result => {
            console.log('SUCCESS: insert ', result);
        });
}

function updateWareHouse(IdMaterial, Qty, IdUser, IdStore) {

     return connection('WareHouse')
        .update('Qty', Qty)
        .where('IdMaterial', IdMaterial)
        .andWhere('IdUser', IdUser)
        .andWhere('IdStore', IdStore)
        .then(result => {
            console.log('SUCCESS: update ', result);
        });;
}

function getDescriptionMaterials(IdMaterial) {

    return connection('Material')
        .select(['Description'])
        .where('IdMaterial', IdMaterial)
        .first();
}

function getNameUsers(IdUser) {

    connection('Users')
        .select(['Name'])
        .where('IdUser', IdUser)
        .first()
        .then(function (use) {
            userd = use.Name
        });
}

function createNotification(IdOrder, IdMaterial, IdSchool, IdZona, Qty, IdUser, IdStore, type, Msg, ownerSchool) {

     connection('Notification')
        .insert({
            IdOrder: IdOrder,
            IdMaterial: IdMaterial,
            IdSchool: IdSchool,
            IdZona: IdZona,
            Qty: Qty,
            IdUser: IdUser,
            IdStore: IdStore,
            Date: new Date(),
            type: type,
            Msg: Msg,
            Owner: ownerSchool,
            Active: true
        })
        .then(result => {
            console.log('SUCCESS: Notification', result);
        });;
}

module.exports = {
    GetOrders
    , InsertOrder
}