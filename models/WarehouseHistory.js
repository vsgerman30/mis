const connection = require('./Connection');
const async = require('async');

function getRows(warehouse, type, next) {

    var data = JSON.parse(warehouse.warehouse);
    var date = new Date().toISOString();

    return connection.select('WareHouseHistory.IdControl', 'Stores.Description AS Tienda', 'Users.Name', 'Material.Description AS Material', 'WareHouseHistory.Qty', 'WareHouseHistory.Date')
        .from('WareHouseHistory')    
        .leftJoin('Stores', 'WareHouseHistory.IdStore', 'Stores.IdStore')
        .leftJoin('Users', 'WareHouseHistory.IdUser', 'Users.IdUser')
        .leftJoin('Material', 'WareHouseHistory.IdMaterial', 'Material.IdMaterial')
        .where('WareHouseHistory.IdStore', data.IdStore)
        .andWhere('WareHouseHistory.IdUser', data.IdUser)
        .andWhere('WareHouseHistory.type', type)
        .whereBetween('WareHouseHistory.Date', [data.Date, date])
        .orderBy('WareHouseHistory.IdControl', 'desc')
        .then(response => next(null, response))
        .catch(err => {
            console.log(err, '  rrr');
        });
}

module.exports = {  
    getRows
};