const connection = require('./Connection');
const async = require('async');

// Functions to retrieve information    
function getReports(report, next) {
    var reports = JSON.parse(report.report);

    reports.values = [];
    reports.labels = [];
    // Create select where Store and dates

    if (reports.Store >= 1 && (reports.sinceDate !== null && reports.untilDate !== null)) {

        if (reports.User >= 1 && reports.School >= 1) {
        // Create select where user and dates

            async.eachOf(reports.Material, (material, key, callback) => {
                connection('Orders')
                    .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                    .sum({ q: 'Qty' })
                    .where('OrderItems.IdMaterial', material)
                    .whereBetween('Orders.Date', [reports.sinceDate, reports.untilDate])
                    .andWhere('Orders.IdUser', reports.User)
                    .andWhere('Orders.IdSchool', reports.School)
                    .andWhere('Orders.IdStore', reports.Store)
                    .then(function (qty) {

                        if (qty && qty[0].q === null) {
                            reports.values.push(0);
                        } else {
                            reports.values.push(qty[0].q);
                        }
                        connection('Material')
                            .select(['Description'])
                            .where('IdMaterial', material)
                            .first()
                            .then(function (Description) {
                                
                                reports.labels.push(Description.Description);
                                callback();
                            })
                            .catch(callback);
                    });
            }, (error) => {
                if (error) {
                    return next(error);
                }
                next(null, reports);
            })
        } else if (reports.User >= 1) {

        // Create full select used to admins reviews
            async.eachOf(reports.Material, (material, key, callback) => {
                connection('Orders')
                    .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                    .sum({ q: 'Qty' })
                    .where('OrderItems.IdMaterial', material)
                    .whereBetween('Orders.Date', [reports.sinceDate, reports.untilDate])
                    .andWhere('Orders.IdUser', reports.User)
                    .andWhere('Orders.IdStore', reports.Store)
                    .then(function (qty) {

                        if (qty && qty[0].q === null) {
                            reports.values.push(0);
                            // return callback();
                        } else {
                            reports.values.push(qty[0].q);
                        }
                        connection('Material')
                            .select(['Description'])
                            .where('IdMaterial', material)
                            .first()
                            .then(function (Description) {
                                reports.labels.push(Description.Description);
                                callback();
                            })
                            .catch(callback);
                            });
                    }, (error) => {
                        if (error) {
                            return next(error);
                        }
                        reports.labels.sort();
                        next(null, reports);
                    });
            } else if (reports.User === 0) {

            // Create full select used just for stores reviews
            async.eachOf(reports.Material, (material, key, callback) => {
                connection('Orders')
                    .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                    .sum({ q: 'Qty' })
                    .where('OrderItems.IdMaterial', material)
                    .whereBetween('Orders.Date', [reports.sinceDate, reports.untilDate])
                    .andWhere('Orders.IdStore', reports.Store)
                    .then(function (qty) {

                        if (qty && qty[0].q === null) {
                            reports.values.push(0);
                            // return callback();
                        } else {
                            reports.values.push(qty[0].q);
                        }
                        connection('Material')
                            .select(['Description'])
                            .where('IdMaterial', material)
                            .first()
                            .then(function (Description) {
                                reports.labels.push(Description.Description);
                                callback();
                            })
                            .catch(callback);
                            });
                    }, (error) => {
                        if (error) {
                            return next(error);
                        }
                        reports.labels.sort();
                        next(null, reports);
                    });
            }
        }
};

function getRows(report, next) {
    var reports = JSON.parse(report.report);

    if (reports.data.Store >= 1 && (reports.data.sinceDate !== null && reports.data.untilDate !== null)) {

        if (reports.data.School >= 1) {
        // Create select where school and dates

            return connection.select('Orders.IdOrder', 'Schools.Description AS School', 'Orders.Student', 'Material.Description AS Material', 'OrderItems.Qty', 'Orders.Date')
                                .from('Orders')    
                                .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                                .leftJoin('Schools', 'Orders.IdSchool', 'Schools.IdSchool')
                                .leftJoin('Material', 'OrderItems.IdMaterial', 'Material.IdMaterial')
                                .whereIn('OrderItems.IdMaterial', reports.data.Material)
                                .whereBetween('Orders.Date', [reports.data.sinceDate, reports.data.untilDate])
                                .andWhere('Orders.IdSchool', reports.data.School)
                                .andWhere('Orders.IdUser', reports.data.User)
                                .andWhere('Orders.IdStore', reports.data.Store)
                                .then(response => next(null, response))
                                .catch(next);

        } else if (reports.data.User >= 1) {
        // Create full select used for admins reviews

            return connection.select('Orders.IdOrder', 'Schools.Description AS School', 'Orders.Student', 'Material.Description AS Material', 'OrderItems.Qty', 'Orders.Date')
                                .from('Orders')    
                                .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                                .leftJoin('Schools', 'Orders.IdSchool', 'Schools.IdSchool')
                                .leftJoin('Material', 'OrderItems.IdMaterial', 'Material.IdMaterial')
                                .whereIn('OrderItems.IdMaterial', reports.data.Material)
                                .whereBetween('Orders.Date', [reports.data.sinceDate, reports.data.untilDate])
                                .andWhere('Orders.IdUser', reports.data.User)
                                .andWhere('Orders.IdStore', reports.data.Store)
                                .then(response => next(null, response))
                                .catch(next);

        } else if (reports.data.User === 0) {
        // Create full select used for stores reviews

            return connection.select('Orders.IdOrder', 'Schools.Description AS School', 'Orders.Student', 'Material.Description AS Material', 'OrderItems.Qty', 'Orders.Date')
                            .from('Orders')    
                            .leftJoin('OrderItems', 'Orders.IdOrder', 'OrderItems.IdOrder')
                            .leftJoin('Schools', 'Orders.IdSchool', 'Schools.IdSchool')
                            .leftJoin('Material', 'OrderItems.IdMaterial', 'Material.IdMaterial')
                            .whereIn('OrderItems.IdMaterial', reports.data.Material)
                            .whereBetween('Orders.Date', [reports.data.sinceDate, reports.data.untilDate])
                            .andWhere('Orders.IdStore', reports.data.Store)
                            .then(response => next(null, response))
                            .catch(next);
        }
    }
};

module.exports = {  
    getReports,
    getRows
};