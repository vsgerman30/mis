const connection = require('./Connection');

// Functions to retrieve information    
function GetAllRoles(){
    return connection.select('*')
                        .from('Roles')
                        .whereNot('IdRole', 3);
};

module.exports = {  
    GetAllRoles
};