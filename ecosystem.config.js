module.exports = {
  apps : [{
    name: 'API',
    script: '/var/www/mis/production/current/bin/www',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'production'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'root',
      host : '45.79.37.236',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:vsgerman30/mis.git',
      path : '/var/www/mis/production',
      'post-deploy' : 'npm install; pm2 startOrReload ecosystem.config.js',
    }
  }
};
