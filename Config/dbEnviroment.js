const config = { 
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
      },
    client: process.env.CLIENT,
    connection: {
        user: process.env.USER,  
        password: process.env.PASSWORD,  
        server: process.env.SERVER,
        database: process.env.DATABASE,
        port: process.env.PORTDB
    }
};  

module.exports=config;
